use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :api13, Api13Web.Endpoint,
  secret_key_base: "d+TvUd9R3vTFsca+ky/vvfDs9BFPDNAC+mRMVtDXHpCb/mEH8h3iu05rchdp3nTi"

# Configure your database
config :api13, Api13.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "api13_prod",
  pool_size: 15
