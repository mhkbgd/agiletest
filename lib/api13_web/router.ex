defmodule Api13Web.Router do
  use Api13Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/test/1", Api13Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api/v1", Api13Web do
    pipe_through :api
    get "/users", UserController, :index
  end

  scope "/api/v1", Api13Web do
    pipe_through :api
    post "/add_users", UserController, :create
  end


  # Other scopes may use custom stacks.
  # scope "/api", Api13Web do
  #   pipe_through :api
  # end
end
